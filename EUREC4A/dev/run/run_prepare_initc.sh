#!/bin/bash

# Retrieve initial conditions

#SBATCH --account=mh0010
#SBATCH --job-name=exp.initc.run
#SBATCH --partition=prepost
#SBATCH --nodes=1
#SBATCH --threads-per-core=2
#SBATCH --output=logs/LOG.exp.initc.run.%j.o
#SBATCH --error=logs/LOG.exp.initc.run.%j.o
#SBATCH --exclusive
#SBATCH --time=01:00:00
#SBATCH --mail-user=hauke.schulz@mpimet.mpg.de
#SBATCH --mail-type=ALL

set -x
module load netcdf_c
/work/mh0010/m300408/anaconda3/envs/ICONrun/bin/python prepare_initc.py -c init-latb-condition_config.yaml

