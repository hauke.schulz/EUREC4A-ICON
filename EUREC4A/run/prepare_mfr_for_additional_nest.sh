#!/bin/bash

# Script that modifies a multifile restartfile in order
# to include an additional nested domain that has not
# been initialized in the initial run of the simulation

# Get restart filename to modify
mfr_tomodify = $0

# Adapt attribute.nc within the restart file
module load nco
ncatted -O -h -a nnew_DOM02,global,c,l,2 attributes.nc
ncatted -O -h -a nnew_rcf_DOM02,global,c,l,2 attributes.nc
ncatted -O -h -a nnow_DOM02,global,c,l,1 attributes.nc
ncatted -O -h -a nnow_rcf_DOM02,global,c,l,1 attributes.nc
ncatted -O -h -a nold_DOM02,global,c,l,3 attributes.nc
ncatted -O -h -a jstep_adv_marchuk_order_DOM02,global,c,l,0 attributes.nc
ncatted -O -h -a n_dom,global,m,l,2 attributes.nc
ncatted -O -h -a ndyn_substeps_DOM02,global,c,l,5 attributes.nc

ncatted -O -h -a t_elapsed_phy_DOM02_PHY01,global,c,d,-999. attributes.nc
ncatted -O -h -a t_elapsed_phy_DOM02_PHY02,global,c,d,1245690. attributes.nc
ncatted -O -h -a t_elapsed_phy_DOM02_PHY03,global,c,d,28688983290. attributes.nc
ncatted -O -h -a t_elapsed_phy_DOM02_PHY04,global,c,d,-999. attributes.nc
ncatted -O -h -a t_elapsed_phy_DOM02_PHY05,global,c,d,-999. attributes.nc
ncatted -O -h -a t_elapsed_phy_DOM02_PHY06,global,c,d,28688983290. attributes.nc
ncatted -O -h -a t_elapsed_phy_DOM02_PHY07,global,c,d,28688983290. attributes.nc
ncatted -O -h -a t_elapsed_phy_DOM02_PHY08,global,c,d,28688983290. attributes.nc
ncatted -O -h -a t_elapsed_phy_DOM02_PHY09,global,c,d,28688983290. attributes.nc
ncatted -O -h -a t_elapsed_phy_DOM02_PHY10,global,c,d,28688983290. attributes.nc

# Output jfiles are not yet set, as I don't understand what they are meant for

# Modify patch data
for patchfile in `ls patch*.nc`
do
  echo $patchfile
  ncrename -O -h -v vn.TL3,vnTL4 $patchfile
  ncrename -O -h -v w.TL3,wTL4 $patchfile
  ncrename -O -h -v theta_v.TL3,thetavTL4 $patchfile
  ncrename -O -h -v rho.TL3,rhoTL4 $patchfile
  ncap2 -h -s "vnTL3=vnTL4*0.;vnTL3=0;wTL3=wTL4*0.;wTL3=0;rhoTL3=rhoTL4*0.;rhoTL3=0;thetavTL3=thetavTL4*0.;thetavTL3=0" $patchfile tmp.nc # . are not allowed in variables names in ncap2
  ncrename -O -h -v vnTL4,vn.TL4 tmp.nc
  ncrename -O -h -v vnTL3,vn.TL3 tmp.nc

  ncrename -O -h -v wTL4,w.TL4 tmp.nc
  ncrename -O -h -v wTL3,w.TL3 tmp.nc

  ncrename -O -h -v rhoTL4,rho.TL4 tmp.nc
  ncrename -O -h -v rhoTL3,rho.TL3 tmp.nc

  ncrename -O -h -v thetavTL4,theta_v.TL4 tmp.nc
  ncrename -O -h -v thetavTL3,theta_v.TL3 tmp.nc

  mv tmp.nc $patchfile
done

