#!/bin/bash

# Retrieve lateral boundary conditions

#SBATCH --account=mh0010
#SBATCH --job-name=exp.latbc.run
#SBATCH --partition=prepost,compute,compute2
#SBATCH --nodes=2
#SBATCH --threads-per-core=2
#SBATCH --output=logs/LOG.exp.latbc.run.%j.o
#SBATCH --error=logs/LOG.exp.latbc.run.%j.o
#SBATCH --exclusive
#SBATCH --time=08:00:00
#SBATCH --mail-user=hauke.schulz@mpimet.mpg.de
#SBATCH --mail-type=ALL

set -x
module load nco
/work/mh0010/m300408/anaconda3/envs/ICONrun/bin/python prepare_latbc.py -c init-latb-condition_config.yaml

