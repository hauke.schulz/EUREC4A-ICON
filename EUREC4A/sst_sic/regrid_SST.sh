#!/bin/bash
# Grep CellArea
cdo -P 8 selname,cell_area ../grids/EUREC4A_PR1250m_DOM01.nc ../grids/EUREC4A_PR1250m_DOM01_CellArea.nc
cdo -P 8 selname,cell_area ../grids/EUREC4A_PR1250m_DOM02.nc ../grids/EUREC4A_PR1250m_DOM02_CellArea.nc
cdo -P 8 selname,cell_area ../grids/EUREC4A_PR1250m_DOM03.nc ../grids/EUREC4A_PR1250m_DOM03_CellArea.nc

cdo -f nc4 remapbil,../grids/EUREC4A_PR1250m_DOM01_CellArea.nc -setgridtype,regular -setmisstonn data/sst_sic.nc data/sst_sic_DOM01.nc
cdo -f nc4 remapbil,../grids/EUREC4A_PR1250m_DOM02_CellArea.nc -setgridtype,regular -setmisstonn data/sst_sic.nc data/sst_sic_DOM02.nc
cdo -f nc4 remapbil,../grids/EUREC4A_PR1250m_DOM03_CellArea.nc -setgridtype,regular -setmisstonn data/sst_sic.nc data/sst_sic_DOM03.nc

