#!/bin/bash
# Regridding data onto regular lat lon grid
#SBATCH --account=mh0010
#SBATCH --job-name=latlon_pp_rad
#SBATCH --partition=compute2,compute
#SBATCH --nodes=15
#SBATCH --threads-per-core=2
#SBATCH --output=logs/LOG.regrid_latlon.%j.o
#SBATCH --error=logs/LOG.regrid_latlon.%j.o
#SBATCH --time=08:00:00
#SBATCH --mail-user=hauke.schulz@mpimet.mpg.de
#SBATCH --mail-type=ALL

experiment=$1
mkdir -p ../../experiments/${experiment}/latlon/

cdo --version
git describe --always --dirty

cd ../../experiments/${experiment}/radiation/
echo $PWD

# Create weights for weighted remapping
if ! [ -f "../../../grids/EUREC4A_PR1250m_DOM01_weights.nc" ]; then
  echo "Create weights for DOM01"
  cdo gendis,../../../grids/lat-lon_DOM01.gridspec -selname,cell_area ../../../grids/EUREC4A_PR1250m_DOM01_CellArea.nc ../../../grids/EUREC4A_PR1250m_DOM01_weights.nc
fi
if ! [ -f "../../../grids/EUREC4A_PR1250m_DOM02_weights.nc" ]; then
  echo "Create weights for DOM02"
  cdo gendis,../../../grids/lat-lon_DOM02.gridspec -selname,cell_area ../../../grids/EUREC4A_PR1250m_DOM02_CellArea.nc ../../../grids/EUREC4A_PR1250m_DOM02_weights.nc
fi

for file in `ls *DOM01*radiation*.nc`; do
  echo Regridding file $file
  if [ -f "../latlon/radiation/${file::${#file}-3}_latlon.nc" ]; then
    echo File already exists. Skip.
  else
    cdo -r -P 30 -f nc4 -z zip remap,../../../grids/lat-lon_DOM01.gridspec,../../../grids/EUREC4A_PR1250m_DOM01_weights.nc -setgrid,../../../grids/EUREC4A_PR1250m_DOM01_CellArea.nc ${file} ../latlon/radiation/${file::${#file}-3}_latlon.nc
    #cdo -r -P 12 -remapdis,../../grids/lat-lon_DOM01.gridspec -setgrid,../../grids/EUREC4A_PR1250m_DOM01.nc ${file} latlon/${file::${#file}-3}_latlon.nc
    echo File written to ../latlon/radiation/${file::${#file}-3}_latlon.nc
  fi
done

for file in `ls *DOM02*radiation*.nc`; do
  echo Regridding file $file
  if [ -f "../latlon/radiation/${file::${#file}-3}_latlon.nc" ]; then
    echo File already exists. Skip.
  else
    cdo -r -P 30 -f nc4 -z zip remap,../../../grids/lat-lon_DOM02.gridspec,../../../grids/EUREC4A_PR1250m_DOM02_weights.nc -setgrid,../../../grids/EUREC4A_PR1250m_DOM02_CellArea.nc ${file} ../latlon/radiation/${file::${#file}-3}_latlon.nc
    #cdo -r -P 12 -remapdis,../../grids/lat-lon_DOM02.gridspec -setgrid,../../grids/EUREC4A_PR1250m_DOM02.nc ${file} latlon/${file::${#file}-3}_latlon.nc
    echo File written to ../latlon/radiation/${file::${#file}-3}_latlon.nc
  fi
done

