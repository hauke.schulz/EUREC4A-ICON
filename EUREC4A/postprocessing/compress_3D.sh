#!/bin/bash
#SBATCH --account=mh0010
#SBATCH --job-name=compress
#SBATCH --partition=prepost
#SBATCH --chdir=/work/mh0010/m300408/DVC-test/EUREC4A-ICON/EUREC4A/experiments/EUREC4A
#SBATCH --nodes=1
#SBATCH --threads-per-core=2
#SBATCH --output=/work/mh0010/m300408/DVC-test/EUREC4A-ICON/EUREC4A/run/logs/LOG.compress_3D.%j.o
#SBATCH --error=/work/mh0010/m300408/DVC-test/EUREC4A-ICON/EUREC4A/run/logs/LOG.compress_3D.%j.o
#SBATCH --exclusive
#SBATCH --time=04:00:00
#SBATCH --mail-user=hauke.schulz@mpimet.mpg.de
#SBATCH --mail-type=ALL
#=============================================================================

mkdir -p /scratch/m/m300408/EUREC4A_Simulation/compressed/

for file in `ls *EUREC4A_DOM02_3D_20200109T*.nc`
do
 echo $file
 nccopy -d1 -s $file /scratch/m/m300408/EUREC4A_Simulation/compressed/${file}
 md5sum $file | cut -d " " -f 1 >> /work/mh0010/m300408/DVC-test/EUREC4A-ICON/EUREC4A/postprocessing/file_hashs_compressed.txt
 #cdo -f nc4 -P 12 -z zip_4 copy -selname,qv $file compressed/${file}4
 #cdo -f nc4 -P 12 copy -selname,qv $file compressed/${file}
done
